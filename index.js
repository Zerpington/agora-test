const express = require('express');
const app = express();

const bodyParser = require('body-parser')
const cors = require('cors');

// database config
const mysql = require('mysql');
const dbConfig = require('./config/db.config');
var db = mysql.createPool(dbConfig.dbConfig);

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json())

app.use(cors())

// app.use("/products/", require("./routes/productsRoute"))

app.get('/', productGet, (req, res) => {
});

app.post('/api/products/', productPost, (req,res) => {
});

app.put('/api/products/:id', productPut, (req,res) => {
});

app.delete('/api/products/:id', productDelete, (req,res) => {
});

function productGet (req, res) {
  db.query('SELECT * FROM Products', function (err, result, fields) {
    if (err) throw err;
    console.log("Get success");
    res.status(200).send(result)
  });
}

function productPost (req, res) {
  product = req.body
  console.log(product)
  db.query(
    'INSERT INTO Products (productName, productPrice) VALUES (?, ?)',
    [
      product.productName, product.productPrice
    ],
    function (err, result, fields) {
    if (err) throw err;
    console.log("Post success")
    res.status(200).send(result)
  });
}

function productPut (req, res) {
  product = req.body
  // console.log(product)
  db.query(
    'UPDATE Products SET productName = ?, productPrice = ? WHERE id = ?',
    [
      product.productName,
      product.productPrice,
      req.params.id
    ],
    function (err, result, fields) {
      if (err) throw err;
      console.log("Put success")
      res.status(200).send(result)
  });
}

function productDelete (req, res) {
  product = req.params
  console.log(product)
  db.query(
    'DELETE FROM `Products` WHERE `id` = ?',
    [product.id],
    function (err, result) {
      console.log("Delete success")
      if (err) throw err;
      res.status(200).send(result)
  });
}

app.listen(3001, () => {
  console.log("running on port 3001")
});
