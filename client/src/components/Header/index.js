import React from 'react';
import './Header.css'

const Header = () => {
  return (
    <div className="header">
      <a className="header-title">AGORA</a>
      <a className="header-item">Home</a>
      <a className="header-item">About</a>
      <a className="header-item">Sign Out</a>
    </div>
  )
}

export default Header
