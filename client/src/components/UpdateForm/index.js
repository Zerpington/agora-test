import React from 'react';
import axios from 'axios';

class UpdateForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      productList: {},
      updateName: "",
      updatePrice: "",
      productId: 0
      }
    this.handlePut = this.handlePut.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleUpdateName = this.handleUpdateName.bind(this);
    this.handleUpdatePrice = this.handleUpdatePrice.bind(this);
  }

  handleUpdateName(e) {
    this.setState({updateName: e.target.value});
  }

  handleUpdatePrice(e) {
    this.setState({updatePrice: e.target.value});
  }

  handlePut(e) {
    e.preventDefault()
    const product= {
      productName: this.state.updateName,
      productPrice: this.state.updatePrice
    }
    var id = e.target.value,
    url = "http://localhost:3001/api/products/"
    axios.put(url + id, product)
    .then((res) => {
      console.log('Post success!')
      this.refreshPage()
    },
    (error) => {
      console.log(error);
    });
  }

  refreshPage() {
    window.location.reload(false);
  }

  handleSubmit(e) {
    e.preventDefault()
    const product = {
      productName: this.state.productName,
      productPrice: this.state.productPrice
    }
    axios.post("http://localhost:3001/api/products/", product)
    .then((res) => {
      console.log('Post success!')
      this.refreshPage()
    },
    (error) => {
      console.log(error);
    });
  }

  handleDelete(e){
    e.preventDefault()
    const id = e.target.value
    var url = "http://localhost:3001/api/products/"
    axios.delete(url + id)
    .then((res) => {
      console.log('Delete success!')
      this.refreshPage()
    },
    (error) => {
      console.log(error);
    });
  }

  componentDidMount() {
    axios.get("http://localhost:3001/")
    .then((res) => {
      const productList = res.data;
      this.setState({productList});
    },
    (error) => {
      console.log(error);
    });
  }

  render(){
    const productList = Array.from(this.state.productList);
    return(
      <ul>
      <input
      type="text"
      // defaultValue=''
      defaultValue={this.state.updateName}
      onChange={this.handleUpdateName}
      required
      />
      <input
      type="text"
      // defaultValue=''
      defaultValue={this.state.updatePrice}
      onChange={this.handleUpdatePrice}
      required
      />
      {productList.map(d => (
        <li key={d.id}>{d.id}
        : {d.productName}
        : P{d.productPrice}
        <button
        type=""
        value={d.id}
        onClick = {this.handleDelete}
        >
        Delete
        </button>
        <button
        type=""
        value={d.id}
        onClick = {this.handlePut}
        >
        Edit
        </button>
        </li>
      ))}
      </ul>
    )
  }
}
export default UpdateForm
