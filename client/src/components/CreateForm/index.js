import React from 'react';
import axios from 'axios';

class CreateForm extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      productName: "",
      productPrice: "",
    }
    this.handleProductName = this.handleProductName.bind(this);
    this.handleProductPrice = this.handleProductPrice.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  refreshPage() {
    window.location.reload(false);
  }

  handleProductName(e) {
    this.setState({productName: e.target.value});
  }

  handleProductPrice(e) {
    this.setState({productPrice: e.target.value});
  }

  handleSubmit(e) {
    e.preventDefault()
    const product = {
      productName: this.state.productName,
      productPrice: this.state.productPrice
    }
    axios.post("http://localhost:3001/api/products/", product)
    .then((res) => {
      console.log('Post success!')
      this.refreshPage()
    },
    (error) => {
      console.log(error);
    });
  }

  render() {
    return (

      <form onSubmit={this.handleSubmit}>
        <div className="form" >

          <label>Product Name:</label>
          <input
            type="text"
            // defaultValue=''
            defaultValue={this.state.productName}
            onChange={this.handleProductName}
            required
          />

          <label>Price</label>
          <input
            type="number"
            // defaultValue=''
            defaultValue={this.state.productPrice}
            onChange={this.handleProductPrice}
            required
          />

          <button
            type="submit"
            value="Submit"
          >
          Submit
          </button>

        </div>
      </form>
    )
  }
}

export default CreateForm
