import React from 'react';
import './App.css';
import axios from 'axios';
import Header from './components/Header'
import CreateForm from './components/CreateForm'
import UpdateForm from './components/UpdateForm'

class App extends React.Component {

  render() {
    return (
      <div className="App">
        <Header />
        <CreateForm />
        <UpdateForm />
      </div>
    );
  }
}

export default App;
