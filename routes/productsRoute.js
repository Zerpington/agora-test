const express = require('express');
const router = express.Router();
productsRoute = require("../controllers/productsControllers")

router.get("/", productsRoute.productsControllers)

module.exports = router;
